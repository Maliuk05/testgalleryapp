import React from 'react';

import { NavigationContainer } from '@react-navigation/native';

import { createStackNavigator } from '@react-navigation/stack';

import { createStore, applyMiddleware} from 'redux';

import { Provider} from 'react-redux';

import thunk from "redux-thunk";

import rootReducer from "./src/store/reducers/rootReducer";

import Layout from "./src/containers/HomeScreen/HomeScreen";
import GalleryItemDetail from "./src/components/GalleryItem/GalleryItemDetail/GalleryItemDetail";

const store = createStore(rootReducer,applyMiddleware(thunk));

const Stack = createStackNavigator();

export default function App() {
  return (
      <Provider store={store}>
          <NavigationContainer>
              <Stack.Navigator>
                  <Stack.Screen name="Gallery App" component={Layout}/>
                  <Stack.Screen name='Photo details' component={GalleryItemDetail}/>
              </Stack.Navigator>
          </NavigationContainer>
      </Provider>
  );
}
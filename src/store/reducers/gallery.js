import {
    FETCH_DATA_ERROR,
    FETCH_DATA_START,
    FETCH_DATA_SUCCESS
} from '../actions/actionsTypes';

const initialState = {
  galleryData: null,
  loading: false,
  error: null
};

export default function galleryReducer(state= initialState,action) {

    switch(action.type) {

        case FETCH_DATA_START:
            return {
                ...state, loading: true
            };
        case FETCH_DATA_SUCCESS:
            return {
                ...state, galleryData: action.data,  loading: false, error: null
            };
        case FETCH_DATA_ERROR:
            return {
                ...state, loading: false,error: action.error
            };
        default:
            return state;
    }
}

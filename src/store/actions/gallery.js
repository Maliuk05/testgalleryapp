import axios from 'axios';
import {
    FETCH_DATA_START,
    FETCH_DATA_SUCCESS,
    FETCH_DATA_ERROR} from "./actionsTypes";

export function fetchData() {
    return async dispatch => {
        dispatch(fetchDataStart());
        try {
            const response = await axios.get('https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0')
            const galleryData = response.data;
            dispatch(fetchDataSuccess(galleryData))
        }catch (e) {
            dispatch(fetchDataError())
        }
    }
}

export function fetchDataStart() {
    return {
        type: FETCH_DATA_START
    }
}

export function fetchDataSuccess(data) {
    return {
        type: FETCH_DATA_SUCCESS,
        data: data
    }
}

export function fetchDataError(e) {
    return {
        type: FETCH_DATA_ERROR,
        error:e
    }
}
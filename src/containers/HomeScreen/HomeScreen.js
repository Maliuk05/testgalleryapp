import React,{useEffect} from 'react';

import { View, ScrollView, StyleSheet } from 'react-native';

import { useDispatch, useSelector } from 'react-redux';

import { fetchData } from "../../store/actions/gallery";

import GalleryItem from "../../components/GalleryItem/GalleryItem";

import Loader from "../../components/Loader/Loader";

import { LinearGradient } from 'expo-linear-gradient';


export default function HomeScreen(props) {

    const galleryData = useSelector(state => state.gallery.galleryData);

    const loading = useSelector(state => state.loading);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchData());
    },[]);

    return (
                <View style={{flex:1}}>
                    {
                    loading || !galleryData
                         ?
                             <Loader/>
                         :
                          <LinearGradient colors={['#9C51B6', '#5946B2']}>
                                   <ScrollView>
                                           <View style={styles.container}>
                                                    {
                                                        galleryData.map( item => {
                                                             return (
                                                                    <GalleryItem
                                                                        key={item.id}
                                                                        username={item.user.name}
                                                                        userThumbImg={item.urls.thumb}
                                                                        userFullImg={item.urls.raw}
                                                                        description={item.description}
                                                                        navigation={props.navigation}
                                                                     />
                                                                )
                                                        })
                                                    }
                                           </View>
                                   </ScrollView>
                          </LinearGradient>
                    }
                </View>
    )

}

const styles = StyleSheet.create({
   container: {
       flex:1,
       flexDirection:'row',
       flexWrap:'wrap',
       justifyContent:'space-around',
       marginBottom:40
   },

});

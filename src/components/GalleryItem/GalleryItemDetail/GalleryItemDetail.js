import React from 'react';
import {View, StyleSheet} from 'react-native';

import FitImage from 'react-native-fit-image';

export default function GalleryItemDetail({route}) {

    const {userFullImg} = route.params;
        return (
            <View style={styles.container}>
                <FitImage
                originalHeight={400}
                originalWidth={600}
                indicator={true}
                indicatorColor="red"
                indicatorSize="large"
                source = {{ uri: `${userFullImg}`}}
                style={styles.fitImage}
                />
            </View>
        )
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#CFCFCF',
        flex: 1
    },
    fitImage: {
        borderRadius: 20,
    },
});
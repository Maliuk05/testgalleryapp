import React from 'react';

import { StyleSheet, Text, View,Image, TouchableOpacity } from 'react-native';

export default function GalleryItem(props)  {

    const { navigate } = props.navigation;
        return (
            <TouchableOpacity onPress={()=> navigate('Photo details', {
                userName: props.username,
                userFullImg:props.userFullImg,
            })}>
            <View style={styles.container}>
                <Image style={styles.tinyLogo}
                    source={{
                         uri: `${props.userThumbImg}`
                    }}
                />
                <Text style={styles.UsernameText}>{props.username}</Text>
                <Text style={styles.imgDescription}>{props.description}</Text>
            </View>
            </TouchableOpacity>
        )
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 50,
        marginRight: 10
    },
    tinyLogo: {
        width: 160,
        height: 160,
    },
    UsernameText: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 16,
        marginTop: 4
    },
    imgDescription: {
        textAlign: 'center',
        fontSize: 14,
        width: 160,
        color: '#C1BAC1',
    }
});
